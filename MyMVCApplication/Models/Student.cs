﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace MyMVCApplication.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        [Display(Name = "Name")]

        [Required(ErrorMessage = "Fuck you.")]
        public string StudentName { get; set; }

        [Range(10, 20)]
        public int Age { get; set; }
        public bool isNewlyEnrolled { get; set; }
        public string Password { get; set; }
        public DateTime DoB { get; set; }

        public Gender StudentGender { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
}